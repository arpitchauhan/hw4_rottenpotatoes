require 'spec_helper'
require 'factory_girl'
FactoryGirl.find_definitions

describe MoviesController do
  render_views
  describe "#show" do
    it 'should render the show template' do
      #fake_result = mock('Movie1')
      movie = FactoryGirl.create(:movie, :id => 1)
      get :show, { :id => 1 }
      response.should render_template('show')
    end 
  end   
  describe "#find_movies_wsd" do
      before(:each) do
        @movie1 = FactoryGirl.create(:movie, :id => 1)
        @movie2 = FactoryGirl.create(:movie, :id => 2, :release_date => '19-May-2003', :rating => 'PG-13')
        @movie2_array = Array.new()
        @movie2_array << @movie2
      end
      
      it 'should call the model method that finds similar movies' do
      
      get :show, { :id => 1 }
      response.should render_template('show')
      Movie.should_receive(:find_movies_wsd).with("1").and_return(@movie2_array)
      
      get :find_movies_wsd, :id => 1
      end  
      
    
    
      
    
    it 'should select the movies with same director view' do
      Movie.stub(:find_movies_wsd).with("1").and_return(@movie2_array)
      get :show, { :id => 1 }
      get :find_movies_wsd, :id => 1
      response.should render_template('find_movies_wsd')
    end
    it 'should make the search results from the model method available to the view' do
      Movie.stub(:find_movies_wsd).with("1").and_return(@movie2_array)
      get :show, { :id => 1 }
      get :find_movies_wsd, :id => 1
      assigns(:movies_wsd).should == @movie2_array
    end
end
end
    
