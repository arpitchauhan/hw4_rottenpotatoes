require 'spec_helper'


describe Movie do
  context ".find_movies_wsd" do
    before(:each) do
      @movie1 = Movie.create!(:id => 1, :director => 'director1')
      @movie2 = Movie.create!(:id => 2, :director => 'director1')
      @movie3 = Movie.create!(:id => 3, :director => 'director2')
      @movie4 = Movie.create!(:id => 4, :director => 'director1')
      @movies_returned = Movie.find_movies_wsd("1")
    end    
    it 'should return the movies with the same director' do
       
      @movies_returned.should be_instance_of(Array)
      @movies_returned.each {should be_instance_of Movie}
      @movies_returned.each {|movie| movie.director == @movie1.director}
    end  
       
  end
end

