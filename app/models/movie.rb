class Movie < ActiveRecord::Base
  def self.all_ratings
    %w(G PG PG-13 NC-17 R)
  end
  def self.find_movies_wsd(id)
    movie = Movie.find_by_id!(id)
    movies_wsd = Movie.find(:all, :conditions => ['director = ? AND id <> ?', movie.director, id]) 
    return movies_wsd
  end
end
