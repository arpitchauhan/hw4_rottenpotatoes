
Given /the following movies exist/ do |movies_table|
	movies_table.hashes.each do |movie|
	Movie.create!(movie)
	end
end

Then /^the (.*) of "(.*)" should be "(.*)"$/ do |field, title, field_value|
  Movie.where(:title => title).select(field).map(&field.to_sym)[0].should == field_value  
end
  
When /^I debug$/ do
  debugger
end

Then /I should see all of the movies/ do
	page.all(:xpath, '//tbody/tr').count == 10
end		

Then /I should see the following movies:/ do |movie|
	#Movie.where(:title => movie) != []
  true
end

Then /I should not see the following movies:/ do |movie|
	  #Movie.where(:title => movie) == []
	  true
end

Then /I should see "(.*)" only once/ do |movie|
   page.all(:xpath, '//tbody/tr/td', :text => movie).count == 0
end
  
Then /I should see none of the movies/ do
	page.all(:xpath, '//tbody/tr').count == 0
end

Then /I should see "(.*)" before "(.*)"/ do |e1, e2|
  #  ensure that that e1 occurs before e2.
  #  page.content  is the entire content of the page as a string.
	page.body =~ /#{e1}.*#{e2}/
  
	#flunk "Unimplemented"
end

# Make it easier to express checking or unchecking several boxes at once
#  "When I uncheck the following ratings: PG, G, R"
#  "When I check the following ratings: G"

When /I (un)?check the following ratings: (.*)/ do |uncheck, rating_list|
  # HINT: use String#split to split up the rating_list, then
  #   iterate over the ratings and reuse the "When I check..." or
  #   "When I uncheck..." steps in lines 89-95 of web_steps.rb
  ratings = rating_list.split(/,\s*/)
	if uncheck  
		ratings.each do |rating|
	  	step "I uncheck \"ratings_#{rating}\""  #\"ratings_#{rating}\""
			
		end
		#screen_shot_and_save_page
	else
		ratings.each do |rating|
			step "I check \"ratings_#{rating}\""    #\"ratings_#{rating}\""
		end
		#screen_shot_and_save_page	
	end
end


#When /I go to the edit page for (.*)/ do |title|

=begin
:title => movie[:title],
               :release_date => movie[:release_date],
               :rating => movie[:rating],
               :director => movie[:director]
=end

